import 'dart:convert';
import 'dart:math';
import 'Models/Ville.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

void main() {
  runApp(new MaterialApp(
    home: Weather(),
  ));
}

Random ran = new Random();

class Weather extends StatefulWidget {
  @override
  _WeatherState createState() => _WeatherState();
}

class _WeatherState extends State<Weather> {
  String temp;
  String ville = "dakar";
  getWeather() async {
    if (click == 1) {
      final response = await http.get(
          "http://api.weatherstack.com/current?access_key=b723bd5d7e5e4e0a20cdfc620ad687a5&query=$ville");

      final responsePhoto = await http.get(
          "https://pixabay.com/api/?key=14448279-e74cbb27d7a35f52337d61dd1&q=$ville");
      final weatherJson = json.decode(response.body);
      final photoJson = json.decode(responsePhoto.body);
      print(photoJson["hits"][ran.nextInt(3)]["largeImageUrl"].toString());
      Ville Tville = new Ville.json(weatherJson);
      Tville.photo =
          photoJson["hits"][ran.nextInt(3)]["largeImageURL"].toString();
      villes.add(Tville);
    }
    click = 0;
  }

  TextEditingController villeController = new TextEditingController();
  List<Ville> villes = new List();
  int click = 0;

//alert dialog
  alertVille() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        villeController.text = "";
        return AlertDialog(
          title: Text('Ville ? :'),
          content: SingleChildScrollView(
              child: Column(
            children: <Widget>[
              TextField(
                controller: villeController,
              )
            ],
          )),
          actions: <Widget>[
            FlatButton(
              child: Text('Valider'),
              onPressed: () {
                ville = villeController.text;
                print(ville);
                click = 1;
                Navigator.of(context).pop();
                setState(() {});
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          bottomOpacity: 5.0,
          title: Text("Weather",style: TextStyle(color: Colors.black),),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            alertVille();
          },
          child: Icon(Icons.add),
        ),
        body: FutureBuilder<dynamic>(
          future: getWeather(), // a previously-obtained Future<String> or null
          builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
                return Text('Press button to start.');
              case ConnectionState.active:
              case ConnectionState.waiting:
                return Center(
                    child: CircularProgressIndicator(
                  backgroundColor: Colors.blue,
                ));
              case ConnectionState.done:
                if (snapshot.hasError) return Text('Error: ${snapshot.error}');
                return listViews();
            }
            return null; // unreachable
          },
        ));
  }

  Widget rowListview(Ville ville) {
    return GestureDetector(
      onTap: () {},
      child: Card(
          elevation: 10.0,
          child: Stack(
            children: <Widget>[
              Container(
                color: Colors.red,
              ),
              Container(
                child: Text(ville.temp),
              ),
              Container(
                child: Text(ville.climat),
              ),
              Container(
                child: Image.network(ville.icon),
              )
            ],
          )),
    );
  }

  Widget listViews() {
    return ListView.builder(
        itemCount: villes.length,
        itemBuilder: (BuildContext context, int position) {
          return GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => Detail(detailVille: villes[position]),
                ),
              );
            },
            child:  Container(
                padding: EdgeInsets.fromLTRB(10, 10, 10, 5),
                  
                  child: Stack(
                    children: <Widget>[
                      ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                                              child: Image.network(
                          villes[position].photo,
                          fit: BoxFit.cover,
                          height: 150.0,
                          width: double.infinity,
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Padding(
                              padding: EdgeInsets.only(left: 5.0, top: 5.0),
                              child: Image.network(villes[position].icon,
                                  width: 50.0, height: 50.0)),
                          SizedBox(
                            width: 20.0,
                          ),
                          Text(
                            villes[position].climat,
                            style: TextStyle(color: Colors.white),
                          )
                        ],
                      ),
                      Positioned(
                        top: 100.0,
                        child: Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Row(
                            children: <Widget>[
                              Text(
                                "${villes[position].temp} ° C",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 20.0),
                              ),
                              SizedBox(
                                width: 200.0,
                              ),
                              Text(
                                villes[position].name,
                                style: TextStyle(
                                    color: Colors.white, fontSize: 20.0),
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  )),
            
          );
        });
  }
}

//detail page

class Detail extends StatelessWidget {
  Ville detailVille = new Ville();
  Detail({Key key, @required this.detailVille}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
      body: GestureDetector(
        onTap: (){
          return Navigator.pop(context);
        },
              child: Stack(
          children: <Widget>[
            Image.network(
              detailVille.photo,
              fit: BoxFit.cover,
              height: double.infinity,
              width: double.infinity,
            ),
           
            Center(
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 100.0,
                  ),
                  Image.network(detailVille.icon, width: 50.0, height: 50.0),
                  SizedBox(
                    height: 40.0,
                  ),
                  Text(
                    detailVille.climat,
                    style: TextStyle(color: Colors.white, fontSize: 20.0),
                  )
                ],
              ),
            ),
            Positioned(
              top: 270.0,
              left: 150.0,
              child: Center(
                child: Column(
                  children: <Widget>[
                    Text(
                      "${detailVille.temp} ° C",
                      style: TextStyle(color: Colors.white, fontSize: 45.0),
                    ),
                    SizedBox(
                      height: 100.0,
                    ),
                    Text(
                      detailVille.name,
                      style: TextStyle(color: Colors.white, fontSize: 25.0),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
