class Ville {
  String name;
  String temp;
  String icon;
  String climat;
  String photo;
  Ville();
  //Ville(this.name ,this.temp,this.icon,this.climat);
  Ville.json(dynamic json){
    temp = json["current"]["temperature"].toString();
    name = json["location"]["name"].toString();
    icon = json["current"]["weather_icons"][0].toString();
    climat = json["current"]["weather_descriptions"][0].toString();
  }
}